package tuke.bc.imageRecognition.factory;

import ij.gui.Roi;
import tuke.bc.imageRecognition.model.Point;
import tuke.bc.imageRecognition.model.Shape;

import java.util.ArrayList;

/**
 *
 * Created by Enfield on 15.03.2016.
 */
public class ShapeFactory {

    public Shape create(ArrayList<Point> points, Roi roi, int colorRGB) {
        return new Shape(points, roi, colorRGB);
    }
}
