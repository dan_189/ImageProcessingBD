package tuke.bc.imageRecognition.model;

import java.awt.*;

/**
 * Created by Enfield on 17.01.2016.
 */
public class TrainerImage {
    private String description;
    private Image image;

    public TrainerImage(String description, Image image) {
        this.description = description;
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public Image getImage() {
        return image;
    }
}
