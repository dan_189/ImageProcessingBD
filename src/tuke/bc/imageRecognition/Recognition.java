package tuke.bc.imageRecognition;

import ij.ImagePlus;
import tuke.bc.imageRecognition.model.Point;
import tuke.bc.imageRecognition.model.Shape;
import tuke.bc.imageRecognition.neuralNetwork.NeuralNetwork;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by Enfield on 24.03.2016.
 * Author: Daniel Bozo
 */
public class Recognition {
    private ArrayList<Shape> recognisedShaped;
    private int backgroundRGB;
    private int letterColorRGB;

    public ImagePlus recognise(ArrayList<Shape> shapes, ImagePlus image) {
        this.backgroundRGB = image.getBufferedImage().getRGB(0, 0);
        recognisedShaped = this.recogniseShapes(shapes, image);
        this.addShapesNameToImage(recognisedShaped, image);
        return image;
    }

    public ArrayList<Shape> recogniseShapes(ArrayList<Shape> shapes, ImagePlus image) {
        NeuralNetwork neuralNetwork = new NeuralNetwork();
        for (Shape shape : shapes) {
            BufferedImage inputImage = this.createInputImage(shape, image);
            String name = neuralNetwork.recognise(inputImage);
            shape.setName(name);
            System.out.println(name);
        }
        return shapes;
    }

    private void addShapesNameToImage(ArrayList<Shape> recognisedShapes, ImagePlus recImage) {
        if (backgroundRGB < new Color(170, 57, 57).getRGB()) {
            letterColorRGB = new Color(255, 255, 255).getRGB();
        } else letterColorRGB = new Color(0, 37, 22).getRGB();
        BufferedImage recognisedImage = recImage.getBufferedImage();
        BufferedImage img = new BufferedImage(recognisedImage.getWidth(), recognisedImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();
        g2d.drawImage(recognisedImage, 0, 0, null);
        g2d.setPaint(new Color(letterColorRGB));
        g2d.setFont(new Font("Arial", Font.BOLD, 12));
        for (Shape shape : recognisedShapes) {
            String name = shape.getName();
            g2d.drawString(name, (int) shape.getArea().getXBase(), (int) shape.getArea().getYBase() - 3);
        }
        g2d.dispose();
        recImage.setImage(img);
    }

    private BufferedImage createInputImage(Shape shape, ImagePlus image) {
        BufferedImage segmentedImage = image.getBufferedImage();
        int whiteRGB = new Color(255, 255, 255).getRGB();
        int blackRGB = new Color(0, 0, 0).getRGB();
        int x = (int) shape.getArea().getBounds().getX();
        int y = (int) shape.getArea().getBounds().getY();
        int width = (int) shape.getArea().getBounds().getWidth();
        int height = (int) shape.getArea().getBounds().getHeight();
        BufferedImage tempImage = segmentedImage.getSubimage(x + 2, y + 2, width - 2, height - 2);
        BufferedImage temp = new BufferedImage(720, 480, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = temp.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g2.setPaint(Color.white);
        g2.fillRect(0, 0, 720, 480);
        g2.drawImage(tempImage, x, y, width, height, null);
        g2.dispose();
        for (int y1 = 0; y1 < temp.getHeight(); y1++) {
            for (int x1 = 0; x1 < temp.getWidth(); x1++) {
                if (x1 >= this.getShapesMaxX(y1, shape) || x1 <= this.getShapesMinX(y1, shape)) {
                    temp.setRGB(x1, y1, whiteRGB);
                } else temp.setRGB(x1, y1, blackRGB);
            }
        }
        BufferedImage newImage = temp.getSubimage(x, y, width, height);
        BufferedImage inputImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = inputImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g.setPaint(Color.white);
        g.fillRect(0, 0, 200, 200);
        g.drawImage(newImage, 50, 50, 100, 100, null, null);
        return inputImage;
    }


    private int getShapesMaxX(int y, Shape shape) {
        int maxX = 0;
        for (Point p : shape.getPoints()) {
            if (p.getY() == y && maxX < p.getX()) {
                maxX = p.getX();
            }
        }
        return maxX;
    }

    private int getShapesMinX(int y, Shape shape) {
        int minX = 1000;
        for (Point p : shape.getPoints()) {
            if (p.getY() == y && minX > p.getX()) {
                minX = p.getX();
            }
        }
        return minX;
    }

    public ArrayList<Shape> getRecognisedShapes() {
        return this.recognisedShaped;
    }

    public int getLetterColorRGB() {
        return this.letterColorRGB;
    }
}