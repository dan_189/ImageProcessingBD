package tuke.bc.imageRecognition.ui.FX;

import ij.ImagePlus;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import tuke.bc.imageRecognition.Recognition;
import tuke.bc.imageRecognition.Replacement;
import tuke.bc.imageRecognition.Segmentation;
import tuke.bc.imageRecognition.model.Shape;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by Enfield on 24.04.2016.
 *
 * @Author D�niel Boz�
 */
public class MainWindow extends Application {
    private ImageView mainImageView;
    private static final String IMAGES_DIRECTORY = System.getProperty("user.home");
    private static final String NEW_OBJECTS_DIRECTORY = System.getProperty("user.home");
    private static final String OUTPUT_IMAGES_DIRECTORY = System.getProperty("user.home");
    private ImagePlus mainImage;
    private java.awt.Image newObjectImage;
    private Button recogniseButton, replaceButton, selectNewObjectButton, saveButton, removeButton;
    private ChoiceBox choiceBox;
    private ArrayList<Shape> recognisedShapes;
    private FileChooser fileChooser;
    private int letterColorRGB;
    private Segmentation segmentation;
    private ArrayList<Integer> shapeRGBs;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("main_window.fxml"));
        primaryStage.setTitle("Image Processing [Daniel Bozo] FEI-->TUKE<-- KPI");
        Scene scene = new Scene(root, 840, 540);
        mainImage = new ImagePlus();
        segmentation = new Segmentation();
        Button openImageButton = (Button) scene.lookup("#openImageButton");
        choiceBox = (ChoiceBox) scene.lookup("#choiceBox");
        choiceBox.setDisable(true);
        recogniseButton = (Button) scene.lookup("#recogniseButton");
        recogniseButton.setDisable(true);
        replaceButton = (Button) scene.lookup("#replaceButton");
        saveButton = (Button) scene.lookup("#saveButton");
        removeButton = (Button) scene.lookup("#removeButton");
        removeButton.setDisable(true);
        saveButton.setDisable(true);
        selectNewObjectButton = (Button) scene.lookup("#selectNewObjectButton");
        selectNewObjectButton.setDisable(true);
        replaceButton.setDisable(true);
        mainImageView = (ImageView) scene.lookup("#mainImageView");
        mainImageView.setFitHeight(480);
        mainImageView.setFitWidth(720);
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image files (*.png)", "*.png");
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(extFilter);
        primaryStage.setScene(scene);
        primaryStage.show();
        openImageButton.setOnAction(event -> this.onOpenImage(primaryStage));
        recogniseButton.setOnAction(event -> this.onRecognise(primaryStage));
        replaceButton.setOnAction(event -> this.onReplace());
        choiceBox.setOnAction(event -> this.handleChoiceBoxEvent());
        selectNewObjectButton.setOnAction(event -> this.onSelectNewObject(primaryStage));
        saveButton.setOnAction(event -> this.onSave(primaryStage));
        removeButton.setOnAction(event -> this.onRemove());
    }


    private void onOpenImage(Stage primaryStage) {
        this.clear();
        fileChooser.setTitle("Select the image");
        fileChooser.setInitialDirectory(new File(IMAGES_DIRECTORY));
        File imageFile = fileChooser.showOpenDialog(primaryStage);
        java.awt.Image im;
        try {
            im = ImageIO.read(imageFile);
            mainImage.setImage(im);
            this.updateMainImageView();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Open image");
            alert.setContentText("Opening the image is failed. Please try again!");
            alert.setHeaderText(null);
            alert.show();
            e.printStackTrace();
        }
        recogniseButton.setDisable(false);
    }

    private void onRecognise(Stage primaryStage) {
        if (mainImage.getHeight() == 0) {
            ButtonType loadButton = new ButtonType("Browse...");
            ButtonType cancelButton = new ButtonType("Cancel");
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Recognise");
            alert.setContentText("Please select your image first!");
            alert.setHeaderText(null);
            alert.getButtonTypes().setAll(loadButton, cancelButton);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == loadButton) {
                this.onOpenImage(primaryStage);
                this.onRecognise(primaryStage);
            } else if (result.get() == cancelButton) {
                alert.close();
            }
            return;
        }
        Recognition recognition = new Recognition();
        segmentation.scan(mainImage.getImage());
        this.shapeRGBs = segmentation.getShapesRGBs();
        ArrayList<Shape> shapes = segmentation.getShapes();
        if (shapes == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Image recognition is failed. \n" +
                    "Error: image segmentation failure.\n" +
                    "Please load again the image!");
            alert.showAndWait();
            this.onOpenImage(primaryStage);
            return;
        }
        mainImage = segmentation.getSegmentedImage();
        mainImage = recognition.recognise(shapes, mainImage);
        this.letterColorRGB = recognition.getLetterColorRGB();
        segmentation.setLettersRGB(this.letterColorRGB);
        recognisedShapes = recognition.getRecognisedShapes();
        if (recognisedShapes == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Image recognition is failed. \n" +
                    "Error: object recognition failure.\n" +
                    "Please load the image again, than run recognition process!");
            alert.showAndWait();
            this.onOpenImage(primaryStage);
            return;
        }
        int i = 1;
        ObservableList<String> observableList = FXCollections.observableArrayList();
        for (Shape shape : recognisedShapes) {
            observableList.add(String.valueOf(i) + ". " + shape.getName());
            i++;
        }
        choiceBox.setItems(observableList);
        this.updateMainImageView();
        recogniseButton.setDisable(true);
        choiceBox.setDisable(false);
        choiceBox.getSelectionModel().clearSelection();
        replaceButton.setDisable(false);
        saveButton.setDisable(false);
        removeButton.setDisable(false);
    }

    private void onReplace() {
        Replacement replacement = new Replacement();
        replacement.setLetterColorRGB(this.letterColorRGB);
        if (choiceBox.getSelectionModel().getSelectedIndex() == -1) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("Please select your object to replace. Use the Choice box bellow Replace button!");
            alert.showAndWait();
            return;
        }
        if (newObjectImage == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("Please select the new object for replace. Use the Select button!");
            alert.showAndWait();
            return;
        }
        int shapeIndex = choiceBox.getSelectionModel().getSelectedIndex();
        mainImage = replacement.replace(recognisedShapes, shapeIndex, mainImage, newObjectImage, this.shapeRGBs);
        if (replacement.getSuccessOfReplacement()) {
            choiceBox.getItems().remove(shapeIndex);
            choiceBox.getSelectionModel().clearSelection();
            this.updateMainImageView();
            this.recogniseButton.setDisable(false);
        }
    }

    private void handleChoiceBoxEvent() {
        Replacement replacement = new Replacement();
        int index = choiceBox.getSelectionModel().getSelectedIndex();
        replacement.markObject(mainImage, recognisedShapes, index);
        this.updateMainImageView();
        selectNewObjectButton.setDisable(false);
    }

    private void updateMainImageView() {
        Image sceneImage = SwingFXUtils.toFXImage((BufferedImage) mainImage.getImage(), null);
        mainImageView.setImage(sceneImage);
    }

    private void onSelectNewObject(Stage primaryStage) {
        fileChooser.setTitle("Select the object");
        fileChooser.setInitialDirectory(new File(NEW_OBJECTS_DIRECTORY));
        File imageFile = fileChooser.showOpenDialog(primaryStage);
        try {
            newObjectImage = ImageIO.read(imageFile);
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Select object");
            alert.setContentText("Opening the image of object is failed. Please try again!");
            alert.setHeaderText(null);
            alert.show();
            e.printStackTrace();
        }
    }

    private void onSave(Stage primaryStage) {
        fileChooser.setTitle("Save Image");
        fileChooser.setInitialDirectory(new File(OUTPUT_IMAGES_DIRECTORY));
        File saveImage = fileChooser.showSaveDialog(primaryStage);
        try {
            ImageIO.write(mainImage.getBufferedImage(), "png", saveImage);
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Save image");
            alert.setContentText("Saving image is failed. Please check the correction of the output file format and try it again!");
            alert.setHeaderText(null);
            alert.show();
            e.printStackTrace();
        }
    }

    private void onRemove() {
        Replacement replacement = new Replacement();
        replacement.setLetterColorRGB(this.letterColorRGB);
        replacement.setShapes(this.recognisedShapes);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        ButtonType yes = new ButtonType("Yes");
        ButtonType cancelButton = new ButtonType("Cancel");
        alert.setTitle("Confirmation");
        alert.setContentText("Are you sure?");
        alert.setHeaderText(null);
        alert.getButtonTypes().setAll(yes, cancelButton);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == yes) {
            replacement.removeObjectNamesAndBoarders(this.mainImage);
            updateMainImageView();
        } else if (result.get() == cancelButton) {
            alert.close();
        }
    }

    private void clear() {
        mainImage = new ImagePlus();
        newObjectImage = null;
        if (recognisedShapes != null) {
            recognisedShapes.clear();
        }
    }
}

