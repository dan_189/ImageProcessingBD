package tuke.bc.imageRecognition;


import tuke.bc.imageRecognition.neuralNetwork.NeuralNetwork;

public class Main {

    public static void main(String[] args) {
        NeuralNetwork neuralNetwork = new NeuralNetwork();
        neuralNetwork.train(5, 63, 63);

    }
}

