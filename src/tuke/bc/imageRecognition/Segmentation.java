package tuke.bc.imageRecognition;

/**
 * Created by Enfield on 24.03.2016.
 *
 * @Author D�niel Boz�
 */

import ij.ImagePlus;
import ij.gui.Roi;
import ij.process.ImageProcessor;
import tuke.bc.imageRecognition.factory.ShapeFactory;
import tuke.bc.imageRecognition.model.Point;
import tuke.bc.imageRecognition.model.Shape;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Segmentation {
    private ArrayList<Shape> shapes = new ArrayList<>();
    private BufferedImage image;
    private ImagePlus segmentedImage;
    private int lettersRGB;
    private int boardRGB;
    private int backgroundRGB;
    private boolean lettersRGBIsDefined = false;
    private ArrayList<Integer> shapesRGBs;
    private boolean firstAdded;


    public void scan(Image image) {
        this.image = (BufferedImage) image;
        this.backgroundRGB = this.image.getRGB(0, 0);
        this.shapesRGBs = new ArrayList<>();
        this.firstAdded = false;
        this.boardRGB = Color.BLUE.getRGB();
        this.shapesRGBs = this.identifyShapeColor();
        ShapeFactory shapeFactory = new ShapeFactory();
        int height = this.image.getHeight();
        int width = this.image.getWidth();
        while (findStartPoint(width, height) != null) {
            Point startPoint = this.findStartPoint(width, height);
            ArrayList<Point> border = this.getBorderPoints(startPoint.getX(), startPoint.getY());
            int bottomY = 0;
            int topY = height;
            int maxX = 0;
            int minX = border.get(0).getX();
            for (Point point : border) {
                int x = point.getX();
                int y = point.getY();
                if (bottomY < y) {
                    bottomY = y;
                }
                if (maxX < x) {
                    maxX = x;
                }
                if (topY > y) {
                    topY = y;
                }
            }
            Roi roi = new Roi(minX - 3, topY - 2, maxX + 2 - minX + 4, bottomY - topY + 4);

            Shape shape = shapeFactory.create(border, roi, this.image.getRGB(startPoint.getX(),startPoint.getY()));
            shapes.add(shape);
        }

        ImagePlus imagePlus = new ImagePlus();
        imagePlus.setImage(image);
        ImageProcessor ip = imagePlus.getProcessor();
        ip.setValue(255);
        for (Shape shape : shapes) {
            ip.draw(shape.getArea());
        }
        segmentedImage = new ImagePlus("Segmented Image", ip);
        //   System.out.println(shapes.size());
    }

    public Point findStartPoint(int width, int height) {
        Point startPoint;
        boolean contains = false;

        for (int x = 1; x < width - 1; x++) {
            for (int y = height - 1; y > 1; y--) {
                Color color = new Color(image.getRGB(x, y));
                if (isShapeColor(color) && !shapes.isEmpty()) {
                    for (Shape shape : shapes) {
                        if (shape.getArea().contains(x, y)) {
                            contains = this.check(x, y, shape);
                        }
                    }
                    if (!contains) {
                        startPoint = new Point(x, y);
                        return startPoint;
                    }
                }
                if (isShapeColor(color) && shapes.isEmpty()) {
                    startPoint = new Point(x, y);
                    return startPoint;
                }
                contains = false;
            }
        }
        return null;
    }

    public ArrayList<Point> getBorderPoints(int startX, int startY) {
        ArrayList<Point> borderPoints = new ArrayList<>();
        Point startPoint = new Point(startX, startY);
        int degree = 90;
        int x, y;
        Point examined;
        borderPoints.add(startPoint);
        //start point found, turn left from 90 degree
        x = startX - 1;
        y = startY;
        examined = new Point(x, y);
        degree += 90;
        Color color = new Color(image.getRGB(examined.getX(), examined.getY()));
        do {
            if (isShapeColor(color) && degree == 90) {
                borderPoints.add(examined);
                degree += 90;
                x = examined.getX() - 1;
                y = examined.getY();
                examined = new Point(x, y);
                color = new Color(image.getRGB(examined.getX(), examined.getY()));
            }
            if (isShapeColor(color) && degree == 180) {
                borderPoints.add(examined);
                degree += 90;
                x = examined.getX();
                y = examined.getY() + 1;
                examined = new Point(x, y);
                color = new Color(image.getRGB(examined.getX(), examined.getY()));
            }
            if (isShapeColor(color) && degree == 270) {
                borderPoints.add(examined);
                degree += 90;
                x = examined.getX() + 1;
                y = examined.getY();
                examined = new Point(x, y);
                color = new Color(image.getRGB(examined.getX(), examined.getY()));
            }
            if (isShapeColor(color) && degree == 360) {
                borderPoints.add(examined);
                degree = 90;
                x = examined.getX();
                y = examined.getY() - 1;
                examined = new Point(x, y);
                color = new Color(image.getRGB(examined.getX(), examined.getY()));
            }
            if (!isShapeColor(color) && degree == 90) {
                degree = 360;
                x = examined.getX() + 1;
                y = examined.getY();
                examined = new Point(x, y);
                color = new Color(image.getRGB(examined.getX(), examined.getY()));
            }
            if (!isShapeColor(color) && degree == 360) {
                degree -= 90;
                x = examined.getX();
                y = examined.getY() + 1;
                examined = new Point(x, y);
                color = new Color(image.getRGB(examined.getX(), examined.getY()));
            }
            if (!isShapeColor(color) && degree == 270) {
                degree -= 90;
                x = examined.getX() - 1;
                y = examined.getY();
                examined = new Point(x, y);
                color = new Color(image.getRGB(examined.getX(), examined.getY()));
            }
            if (!isShapeColor(color) && degree == 180) {
                degree -= 90;
                x = examined.getX();
                y = examined.getY() - 1;
                examined = new Point(x, y);
                color = new Color(image.getRGB(examined.getX(), examined.getY()));
            }
        } while (examined.getX() != startPoint.getX() || examined.getY() != startPoint.getY());
        return borderPoints;
    }

    public boolean isShapeColor(Color color) {
        for (int rgb : shapesRGBs) {
            if (rgb == color.getRGB())
                return true;
        }
        return false;
    }

    public ArrayList<Shape> getShapes() {
        return shapes;
    }

    public ImagePlus getSegmentedImage() {
        return this.segmentedImage;
    }

    public boolean check(int x, int y, Shape shape) {
        return (!(shape.getPointForCheck(x, y) != null && shape.getPointForCheck(x, y).getX() < x));
    }

    private ArrayList<Integer> identifyShapeColor() {
        Set<Integer> has = new HashSet<>();
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int rgb = image.getRGB(x, y);
                if (!lettersRGBIsDefined) {
                    if (!firstAdded) {
                        if (rgb != this.backgroundRGB) {
                            shapesRGBs.add(rgb);
                            this.firstAdded = true;
                        }
                    } else {
                        if (shapesRGBs.get(shapesRGBs.size() - 1) != rgb && rgb != backgroundRGB) {
                            shapesRGBs.add(rgb);
                        }
                    }
                } else {
                    if (rgb != backgroundRGB && rgb != boardRGB && rgb != lettersRGB) {
                        if (!shapesRGBs.isEmpty() && shapesRGBs.get(shapesRGBs.size() - 1) != rgb) {
                            shapesRGBs.add(rgb);
                        }
                        if (shapesRGBs.isEmpty()) {
                            shapesRGBs.add(rgb);
                        }
                    }
                }
            }
        }
        has.addAll(shapesRGBs);
        shapesRGBs.clear();
        shapesRGBs.addAll(has);
        System.out.println(shapesRGBs.size());
        return shapesRGBs;
    }

    public ArrayList<Integer> getShapesRGBs() {
        return this.shapesRGBs;
    }

    public void setLettersRGB(int RGB) {
        this.lettersRGB = RGB;
        this.lettersRGBIsDefined = true;
    }
}

