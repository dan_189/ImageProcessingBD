package tuke.bc.imageRecognition.model;

/**
 *
 * Created by Enfield on 15.03.2016.
 */
public class Point {

    private int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }
}
