package tuke.bc.imageRecognition.model;

import ij.gui.Roi;

import java.util.ArrayList;

/**
 * Created by Enfield on 15.03.2016.
 */
public class Shape  {

    private ArrayList<Point> points;
    private Roi area;
    private String name;
    private int colorRGB;

    public Shape(ArrayList<Point> points, Roi area, int colorRGB) {
        this.points = points;
        this.area = area;
        this.colorRGB = colorRGB;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Point> getPoints() {
        return this.points;
    }

    public Roi getArea() {
        return this.area;
    }

    public String getName() {
        return this.name;
    }

    public int getColorRGB() {
        return colorRGB;
    }

    public Point getPointForCheck(int x, int y) {
        int maxY = 0;
        int maxX = 0;
        for (Point p : points) {
            if (maxX < p.getX() && p.getY() == y) {
                maxX = p.getX();
                maxY = p.getY();
            }
        }
        if (maxX != 0) {
            return new Point(maxX, maxY);
        }
        return null;
    }

}
