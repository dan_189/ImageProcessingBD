package tuke.bc.imageRecognition;

import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import javafx.scene.control.Alert;
import tuke.bc.imageRecognition.model.Point;
import tuke.bc.imageRecognition.model.Shape;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by Enfield on 26.03.2016.
 * Author: Daniel Bozo
 */
public class Replacement {

    private Roi areaOfNewShape;
    private Overlay overlay;
    private ImagePlus segmentedImage;
    private Point firstCenterPoint;
    private BufferedImage image;
    private ArrayList<Point> newShapesOldPoints = new ArrayList<>();
    private ArrayList<Shape> shapes;
    private int oldShapeIndex;
    private int backgroundRGB;
    private int letterColorRGB;
    private boolean outOfSpace;
    private ArrayList<Point> newShapesNewPoints;
    private boolean checked;
    private boolean replaceDone;
    private ArrayList<Integer> shapeRGBs;
    private int oldShapeRGB;

    public ImagePlus replace(ArrayList<Shape> shapes, int oldShapeIndex, ImagePlus segmentedImage, Image newShapeImage, ArrayList<Integer> shapeRGBs) {
        this.shapeRGBs = shapeRGBs;
        this.segmentedImage = segmentedImage;
        this.shapes = shapes;
        this.checked = false;
        this.outOfSpace = false;
        this.oldShapeIndex = oldShapeIndex;
        this.oldShapeRGB = shapes.get(oldShapeIndex).getColorRGB();
        this.backgroundRGB = this.identifyBackgroundRGB();
        this.overlay = new Overlay();
        this.image = segmentedImage.getBufferedImage();
        this.replaceDone = false;
        Shape oldShape = this.shapes.get(oldShapeIndex);
        firstCenterPoint = getShapesCenterPoint(oldShape);
        addNewShapeToImage(newShapeImage);
        this.segmentedImage.setOverlay(overlay);
        return this.segmentedImage;
    }

    public Point getShapesCenterPoint(Shape shape) {
        int x = 0, y = 0;
        try {
            Roi area = shape.getArea();
            x = (int) (area.getXBase() + (area.getFloatWidth() / 2));
            y = (int) (area.getYBase() + (area.getFloatHeight() / 2));
        } catch (NullPointerException e) {
            System.out.println("'getShapesCenterPoint' - defined shape not contains Roi object - " +
                    "can't calculate center point ");
            e.printStackTrace();
        }
        return new Point(x, y);
    }

    public Roi getAreaOfNewShape(Image newShapeImage) {
        try {
            Segmentation segmentation = new Segmentation();
            segmentation.scan(newShapeImage);
            BufferedImage bufferedShapeImage = (BufferedImage) newShapeImage;
            Shape newShape = segmentation.getShapes().get(0);
            areaOfNewShape = newShape.getArea();
            for (int y = 0; y < bufferedShapeImage.getHeight(); y++) {
                for (int x = 0; x < bufferedShapeImage.getWidth(); x++) {
                    Color c = new Color(bufferedShapeImage.getRGB(x, y));
                    if (c.getRGB() == Color.BLACK.getRGB())
                        newShapesOldPoints.add(new Point(x, y));
                }
            }
        } catch (NullPointerException e) {
            System.out.println("'getAreaOfNewShape' - newShapeImage is null");
            e.printStackTrace();
        }
        return areaOfNewShape;
    }

    public void deleteOldShapeFromImage(Shape oldShape) {
        int yBase = (int) oldShape.getArea().getYBase();
        int xBase = (int) oldShape.getArea().getXBase();
        int areaHeight = (int) oldShape.getArea().getFloatHeight();
        int areaWidth = (int) oldShape.getArea().getFloatWidth();
        ArrayList<Point> lettersPoints = this.getLetterPoints(xBase, yBase, segmentedImage);

        for (int y1 = yBase; y1 < yBase + areaHeight; y1++) {
            for (int x1 = xBase + 1; x1 < xBase + areaWidth; x1++) {
                if (x1 <= this.getShapesMaxX(y1, oldShape) && x1 >= this.getShapesMinX(y1, oldShape))
                    image.setRGB(x1, y1, backgroundRGB);
            }
        }
        for (Point p : lettersPoints) {
            image.setRGB(p.getX(), p.getY(), backgroundRGB);
        }
        System.out.println(lettersPoints.size());
        Roi areaOfOldShape = oldShape.getArea();
        segmentedImage.setOverlay(overlay);
        segmentedImage.getProcessor().setColor(new Color(this.backgroundRGB));
        areaOfOldShape.drawPixels(segmentedImage.getProcessor());
    }

    public void addNewShapeToImage(Image newShapeImage) {
        this.checkSpaceToDraw(newShapeImage);
        if (outOfSpace) {
            return;
        }
        checked = true;
        this.deleteOldShapeFromImage(shapes.get(oldShapeIndex));
        this.checkSpaceToDraw(newShapeImage);
        this.shapes.remove(oldShapeIndex);
        this.shapes.add(new Shape(newShapesNewPoints, areaOfNewShape, oldShapeRGB));
        System.out.println(areaOfNewShape.getXBase());
        this.fixShapes(this.image);
        overlay.add(areaOfNewShape);
        this.replaceDone = true;
    }

    public boolean isShapePoint(Point point) {
        for (Point p : newShapesOldPoints) {
            if (p.getX() == point.getX() && p.getY() == point.getY())
                return true;
        }
        return false;
    }

    public ArrayList<Point> getLetterPoints(int xBase, int yBase, ImagePlus segmentedImage) {
        image = (BufferedImage) segmentedImage.getImage();
        ArrayList<Point> lettersPoints = new ArrayList<>();
        for (int y = yBase; y > yBase - 15; y--) {
            for (int x = xBase; x < xBase + 70; x++) {
                if (image.getRGB(x, y) == this.letterColorRGB)
                    lettersPoints.add(new Point(x, y));
            }
        }
        return lettersPoints;
    }

    private void checkSpaceToDraw(Image newShapeImage) {
        areaOfNewShape = getAreaOfNewShape(newShapeImage);
        int imageWidth = newShapeImage.getWidth(null);
        int imageHeight = newShapeImage.getHeight(null);
        int areaWidth = (int) areaOfNewShape.getFloatWidth();
        int areaHeight = (int) areaOfNewShape.getFloatHeight();
        int oldXBase = (int) areaOfNewShape.getXBase();
        int oldYBase = (int) areaOfNewShape.getYBase();
        areaOfNewShape.setStroke(new BasicStroke());
        areaOfNewShape.setLocation(firstCenterPoint.getX() - areaWidth / 2, firstCenterPoint.getY() - areaHeight / 2);
        int newXBase = (int) areaOfNewShape.getXBase();
        int newYBase = (int) areaOfNewShape.getYBase();
        newShapesNewPoints = new ArrayList<>();
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                if (this.isShapePoint(new Point(x, y))) {
                    if (!checked) {
                        try {
                            image.setRGB(x - oldXBase + newXBase, y - oldYBase + newYBase, image.getRGB(x - oldXBase + newXBase, y - oldYBase + newYBase));
                        } catch (ArrayIndexOutOfBoundsException e) {
                            outOfSpace = true;
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setContentText("Not enoght space on the image to draw new object.\n" +
                                    "Please select another object!");
                            alert.setHeaderText(null);
                            alert.showAndWait();
                            return;
                        }
                    } else {
                        image.setRGB(x - oldXBase + newXBase, y - oldYBase + newYBase, oldShapeRGB);
                        newShapesNewPoints.add(new Point(x - oldXBase + newXBase, y - oldYBase + newYBase));
                    }
                }
            }
        }
    }

    private int getShapesMaxX(int y, Shape shape) {
        int maxX = 0;
        for (Point p : shape.getPoints()) {
            if (p.getY() == y && maxX < p.getX()) {
                maxX = p.getX();
            }
        }
        return maxX;
    }

    private int getShapesMinX(int y, Shape shape) {
        int minX = 10000;
        for (Point p : shape.getPoints()) {
            if (p.getY() == y && minX > p.getX()) {
                minX = p.getX();
            }
        }
        return minX;
    }

    private void fixShapes(BufferedImage image) {

        for (Shape oldShape : this.shapes) {
            int yBase = (int) oldShape.getArea().getYBase();
            int xBase = (int) oldShape.getArea().getXBase();
            int areaHeight = (int) oldShape.getArea().getFloatHeight();
            int areaWidth = (int) oldShape.getArea().getFloatWidth();

            for (int y1 = yBase; y1 < yBase + areaHeight; y1++) {
                for (int x1 = xBase + 1; x1 < xBase + areaWidth; x1++) {
                    if (x1 <= this.getShapesMaxX(y1, oldShape) && x1 >= this.getShapesMinX(y1, oldShape))
                        image.setRGB(x1, y1, oldShape.getColorRGB());
                }
            }
        }
    }

    public void markObject(ImagePlus recognisedImage, ArrayList<Shape> shapes, int index) {
        recognisedImage.setOverlay(overlay);
        for (Shape shape : shapes) {
            Roi areaOfShape = shape.getArea();
            if (shapes.indexOf(shape) == index) {
                recognisedImage.getProcessor().setColor(new Color(255,6,6));
                areaOfShape.drawPixels(recognisedImage.getProcessor());
            } else {
                recognisedImage.getProcessor().setColor(Color.BLUE);
                areaOfShape.drawPixels(recognisedImage.getProcessor());
            }
        }
    }

    private int identifyBackgroundRGB() {
        return segmentedImage.getBufferedImage().getRGB(0, 0);
    }

    public void setLetterColorRGB(int letterColorRGB) {
        this.letterColorRGB = letterColorRGB;
    }

    public void removeObjectNamesAndBoarders(ImagePlus imageP) {
        BufferedImage image = imageP.getBufferedImage();
        int backgroundRGB = image.getRGB(0, 0);
        int boardRGB = Color.BLUE.getRGB();
        int markedBoardRGB = new Color(255,6,6).getRGB();
          for (int y = 0; y < image.getHeight(); y++) {
              for (int x = 0; x < image.getWidth(); x++) {
                  if (image.getRGB(x, y) == this.letterColorRGB || image.getRGB(x, y) == boardRGB||
                          image.getRGB(x,y)==markedBoardRGB) {
                      image.setRGB(x, y, backgroundRGB);
                  }
              }
          }
        this.fixShapes(image);
        imageP.setImage(image);
    }

    public void setShapes(ArrayList<Shape> shapes) {
        this.shapes = shapes;
    }

    public boolean getSuccessOfReplacement() {
        return this.replaceDone;
    }


}