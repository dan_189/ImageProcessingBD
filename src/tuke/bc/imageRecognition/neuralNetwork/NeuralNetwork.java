package tuke.bc.imageRecognition.neuralNetwork;

import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.train.strategy.ResetStrategy;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.platformspecific.j2se.data.image.ImageMLData;
import org.encog.platformspecific.j2se.data.image.ImageMLDataSet;
import org.encog.util.downsample.Downsample;
import org.encog.util.downsample.RGBDownsample;
import org.encog.util.simple.EncogUtility;
import tuke.bc.imageRecognition.model.TrainerImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NeuralNetwork {

    private RGBDownsample downSample = new RGBDownsample();
    private BasicNetwork network;

    public void train(int objectCount, int firstHiddenLayerNeuronCount, int secondHiddenLayerNeuronCount) {
        ArrayList<TrainerImage> trainerImages = this.createTrainingData();
        ImageMLDataSet training = setNeurons(downSample, objectCount, trainerImages);
        System.out.print("\ndataset done!!!\n");

        network = EncogUtility.simpleFeedForward(training.getInputSize(), firstHiddenLayerNeuronCount,
                secondHiddenLayerNeuronCount, training.getIdealSize(), true);
        System.out.print("\nNetwork done" + network.toString() + "\n");

        ResilientPropagation train = new ResilientPropagation(network, training);
        train.addStrategy(new ResetStrategy(0.5, 10));
        EncogUtility.trainConsole(train, network, training, 1);
        System.out.print("\nTraining stopped\n");
        InputStream input = NeuralNetwork.class.getResourceAsStream("/tuke/bc/imageRecognition/neuralNetwork/network#1");
        EncogDirectoryPersistence.saveObject(new File("/tuke/bc/imageRecognition/neuralNetwork/network#1"), network);
    }

    public String recognise(BufferedImage image) {
        InputStream input = NeuralNetwork.class.getResourceAsStream("/tuke/bc/imageRecognition/neuralNetwork/network#1");
        if ((network = (BasicNetwork) EncogDirectoryPersistence.loadObject(input)) == null) {
            this.train(5, 63, 63);
            this.recognise(image);
        }
        ImageMLData inputImage = new ImageMLData(image);
        inputImage.downsample(downSample, true, 16, 16, 1, -1);
        int winner = network.winner(inputImage);
        switch (winner) {
            case 0:
                return "circle";
            case 1:
                return "triangle";
            case 2:
                return "square";
            case 3:
                return "star";
            case 4:
                return "arrow";
            default:
                break;
        }
        return null;
    }


    private ImageMLDataSet setNeurons(Downsample downS, int count, ArrayList<TrainerImage> images) {
        ImageMLDataSet mTrainingData = new ImageMLDataSet(downS, true, 1, -1);
        ArrayList<String> list = new ArrayList<>();
        int requiredOutputNeurons = 0;
        for (TrainerImage image : images) {
            String description = image.getDescription();
            for (TrainerImage image1 : images) {
                if (image1.getDescription().equals(description) && !list.contains(description)) {
                    MLData ideal = new BasicMLData(count);
                    ideal.setData(requiredOutputNeurons, 1);
                    for (int otherNeurons = 0; otherNeurons < count; otherNeurons++) {
                        if (otherNeurons != requiredOutputNeurons) ideal.setData(otherNeurons, -1);
                    }
                    ImageMLData data = new ImageMLData(image1.getImage());
                    mTrainingData.add(data, ideal);
                }
            }
            if (!list.contains(description)) {
                list.add(description);
                requiredOutputNeurons++;
            }
        }
        mTrainingData.downsample(16, 16);
        return mTrainingData;
    }

    public ArrayList<TrainerImage> createTrainingData() {
        Image image = null, image2 = null, image3 = null,
                image4 = null, image5 = null,
                image6 = null, image7 = null, image8 = null, image9 = null, image10 = null,
                image11 = null, image12 = null, image13 = null, image14 = null, image15 = null,
                image16 = null, image17 = null, image18 = null, image19 = null, image20 = null,
                image21 = null, image22 = null, image23 = null, image24 = null, image25 = null,
                image26 = null, image27 = null, image28 = null, image29 = null, image30 = null,
                image31 = null, image32 = null, image33 = null, image34 = null, image35 = null,
                image36 = null, image37 = null, image38 = null, image39 = null, image40 = null,
                image41 = null, image42 = null, image43 = null, image44 = null, image45 = null,
                image46 = null, image47 = null, image48 = null, image49 = null, image50 = null,
                image51 = null, image52 = null, image53 = null, image54 = null, image55 = null,
                image56 = null, image57 = null, image58 = null, image59 = null, image60 = null,
                image61 = null, image62 = null, image63 = null, image64 = null, image65 = null,
                image66 = null, image67 = null, image68 = null, image69 = null, image70 = null,
                image72 = null, image73 = null, image74 = null, image75 = null, image76 = null,
                image77 = null, image78 = null, image79 = null, image80 = null, image81 = null,
                image82 = null, image83 = null, image84 = null, image85 = null, image86 = null,
                image87 = null, image88 = null, image89 = null, image90 = null, image91 = null,
                image92 = null, image93 = null, image94 = null, image95 = null, image96 = null,
                image97 = null, image98 = null, image99 = null, image100 = null, image101 = null,
                image103 = null, image104 = null, image105 = null, image106 = null, image107 = null,
                image108 = null, image109 = null, image110 = null, image102 = null, image71 = null,
                image111 = null, image112 = null, image113 = null, image114 = null, image115 = null, image116 = null,
                image117 = null, image118 = null, image119 = null, image120 = null, image121 = null, image124 = null,
                image122 = null, image123 = null, image126 = null, image125 = null, image127 = null, image128 = null, image129 = null,
                image130 = null;

        try {
            image = ImageIO.read(new File("images/image#1.png"));
            image2 = ImageIO.read(new File("images/image#2.png"));
            image3 = ImageIO.read(new File("images/image#3.png"));
            image4 = ImageIO.read(new File("images/image#4.png"));
            image9 = ImageIO.read(new File("images/image#9.png"));
            image5 = ImageIO.read(new File("images/image#5.png"));
            image6 = ImageIO.read(new File("images/image#6.png"));
            image7 = ImageIO.read(new File("images/image#7.png"));
            image8 = ImageIO.read(new File("images/image#8.png"));
            image10 = ImageIO.read(new File("images/image#10.png"));
            image11 = ImageIO.read(new File("images/image#11.png"));
            image12 = ImageIO.read(new File("images/image#12.png"));
            image13 = ImageIO.read(new File("images/image#13.png"));
            image14 = ImageIO.read(new File("images/image#14.png"));
            image15 = ImageIO.read(new File("images/image#15.png"));
            image16 = ImageIO.read(new File("images/image#16.png"));
            image17 = ImageIO.read(new File("images/image#17.png"));
            image18 = ImageIO.read(new File("images/image#18.png"));
            image19 = ImageIO.read(new File("images/image#19.png"));
            image20 = ImageIO.read(new File("images/image#20.png"));
            image21 = ImageIO.read(new File("images/image#21.png"));
            image22 = ImageIO.read(new File("images/image#22.png"));
            image23 = ImageIO.read(new File("images/image#23.png"));
            image24 = ImageIO.read(new File("images/image#24.png"));
            image25 = ImageIO.read(new File("images/image#25.png"));
            image26 = ImageIO.read(new File("images/image#26.png"));
            image27 = ImageIO.read(new File("images/image#27.png"));
            image28 = ImageIO.read(new File("images/image#28.png"));
            image29 = ImageIO.read(new File("images/image#29.png"));
            image30 = ImageIO.read(new File("images/image#30.png"));
            image31 = ImageIO.read(new File("images/image#31.png"));
            image32 = ImageIO.read(new File("images/image#32.png"));
            image33 = ImageIO.read(new File("images/image#33.png"));
            image34 = ImageIO.read(new File("images/image#34.png"));
            image35 = ImageIO.read(new File("images/image#35.png"));
            image36 = ImageIO.read(new File("images/image#36.png"));
            image37 = ImageIO.read(new File("images/image#37.png"));
            image38 = ImageIO.read(new File("images/image#38.png"));
            image39 = ImageIO.read(new File("images/image#39.png"));
            image40 = ImageIO.read(new File("images/image#40.png"));
            image41 = ImageIO.read(new File("images/image#41.png"));
            image42 = ImageIO.read(new File("images/image#42.png"));
            image43 = ImageIO.read(new File("images/image#43.png"));
            image44 = ImageIO.read(new File("images/image#44.png"));
            image45 = ImageIO.read(new File("images/image#45.png"));
            image46 = ImageIO.read(new File("images/image#46.png"));
            image47 = ImageIO.read(new File("images/image#47.png"));
            image48 = ImageIO.read(new File("images/image#48.png"));
            image49 = ImageIO.read(new File("images/image#49.png"));
            image50 = ImageIO.read(new File("images/image#50.png"));
            image51 = ImageIO.read(new File("images/image#51.png"));
            image52 = ImageIO.read(new File("images/image#52.png"));
            image53 = ImageIO.read(new File("images/image#53.png"));
            image54 = ImageIO.read(new File("images/image#54.png"));
            image55 = ImageIO.read(new File("images/image#55.png"));
            image56 = ImageIO.read(new File("images/image#56.png"));
            image57 = ImageIO.read(new File("images/image#57.png"));
            image58 = ImageIO.read(new File("images/image#58.png"));
            image59 = ImageIO.read(new File("images/image#59.png"));
            image60 = ImageIO.read(new File("images/image#60.png"));
            image61 = ImageIO.read(new File("images/image#61.png"));
            image62 = ImageIO.read(new File("images/image#62.png"));
            image63 = ImageIO.read(new File("images/image#63.png"));
            image64 = ImageIO.read(new File("images/image#64.png"));
            image65 = ImageIO.read(new File("images/image#65.png"));
            image66 = ImageIO.read(new File("images/image#66.png"));
            image67 = ImageIO.read(new File("images/image#67.png"));
            image68 = ImageIO.read(new File("images/image#68.png"));
            image69 = ImageIO.read(new File("images/image#69.png"));
            image70 = ImageIO.read(new File("images/image#70.png"));
            image71 = ImageIO.read(new File("images/image#71.png"));
            image72 = ImageIO.read(new File("images/image#72.png"));
            image73 = ImageIO.read(new File("images/image#73.png"));
            image74 = ImageIO.read(new File("images/image#74.png"));
            image75 = ImageIO.read(new File("images/image#75.png"));
            image76 = ImageIO.read(new File("images/image#76.png"));
            image77 = ImageIO.read(new File("images/image#77.png"));
            image78 = ImageIO.read(new File("images/image#78.png"));
            image79 = ImageIO.read(new File("images/image#79.png"));
            image80 = ImageIO.read(new File("images/image#80.png"));
            image81 = ImageIO.read(new File("images/image#81.png"));
            image82 = ImageIO.read(new File("images/image#82.png"));
            image83 = ImageIO.read(new File("images/image#83.png"));
            image84 = ImageIO.read(new File("images/image#84.png"));
            image85 = ImageIO.read(new File("images/image#85.png"));
            image86 = ImageIO.read(new File("images/image#86.png"));
            image87 = ImageIO.read(new File("images/image#87.png"));
            image88 = ImageIO.read(new File("images/image#88.png"));
            image89 = ImageIO.read(new File("images/image#89.png"));
            image90 = ImageIO.read(new File("images/image#90.png"));
            image91 = ImageIO.read(new File("images/image#91.png"));
            image92 = ImageIO.read(new File("images/image#92.png"));
            image93 = ImageIO.read(new File("images/image#93.png"));
            image94 = ImageIO.read(new File("images/image#94.png"));
            image95 = ImageIO.read(new File("images/image#95.png"));
            image96 = ImageIO.read(new File("images/image#96.png"));
            image97 = ImageIO.read(new File("images/image#97.png"));
            image98 = ImageIO.read(new File("images/image#98.png"));
            image99 = ImageIO.read(new File("images/image#99.png"));
            image100 = ImageIO.read(new File("images/image#100.png"));
            image101 = ImageIO.read(new File("images/image#101.png"));
            image102 = ImageIO.read(new File("images/image#102.png"));
            image103 = ImageIO.read(new File("images/image#103.png"));
            image104 = ImageIO.read(new File("images/image#104.png"));
            image105 = ImageIO.read(new File("images/image#105.png"));
            image106 = ImageIO.read(new File("images/image#106.png"));
            image107 = ImageIO.read(new File("images/image#107.png"));
            image108 = ImageIO.read(new File("images/image#108.png"));
            image109 = ImageIO.read(new File("images/image#109.png"));
            image110 = ImageIO.read(new File("images/image#110.png"));
            image111 = ImageIO.read(new File("images/image#111.png"));
            image112 = ImageIO.read(new File("images/image#112.png"));
            image113 = ImageIO.read(new File("images/image#113.png"));
            image114 = ImageIO.read(new File("images/image#114.png"));
            image115 = ImageIO.read(new File("images/image#115.png"));
            image116 = ImageIO.read(new File("images/image#116.png"));
            image117 = ImageIO.read(new File("images/image#117.png"));
            image118 = ImageIO.read(new File("images/image#118.png"));
            image119 = ImageIO.read(new File("images/image#119.png"));
            image120 = ImageIO.read(new File("images/image#120.png"));
            image121 = ImageIO.read(new File("images/image#121.png"));
            image122 = ImageIO.read(new File("images/image#121.png"));
            image123 = ImageIO.read(new File("images/image#121.png"));
            image124 = ImageIO.read(new File("images/image#121.png"));
            image125 = ImageIO.read(new File("images/image#121.png"));
            image126 = ImageIO.read(new File("images/image#121.png"));
            image127 = ImageIO.read(new File("images/image#121.png"));
            image128 = ImageIO.read(new File("images/image#121.png"));
            image129 = ImageIO.read(new File("images/image#121.png"));
            image130 = ImageIO.read(new File("images/image#121.png"));


        } catch (IOException ex) {
            Logger.getLogger(NeuralNetwork.class.getName()).log(Level.SEVERE, null, ex);
        }

        ArrayList<TrainerImage> trainerImages = new ArrayList<>();
        trainerImages.add(new TrainerImage("circle", image));
        trainerImages.add(new TrainerImage("circle", image2));
        trainerImages.add(new TrainerImage("circle", image3));
        trainerImages.add(new TrainerImage("circle", image4));
        trainerImages.add(new TrainerImage("circle", image5));
        trainerImages.add(new TrainerImage("circle", image6));
        trainerImages.add(new TrainerImage("circle", image7));
        trainerImages.add(new TrainerImage("circle", image8));
        trainerImages.add(new TrainerImage("circle", image9));
        trainerImages.add(new TrainerImage("circle", image10));
        trainerImages.add(new TrainerImage("circle", image11));
        trainerImages.add(new TrainerImage("circle", image12));
        trainerImages.add(new TrainerImage("circle", image13));
        trainerImages.add(new TrainerImage("circle", image14));
        trainerImages.add(new TrainerImage("circle", image15));
        trainerImages.add(new TrainerImage("circle", image16));
        trainerImages.add(new TrainerImage("circle", image17));
        trainerImages.add(new TrainerImage("circle", image18));
        trainerImages.add(new TrainerImage("circle", image19));
        trainerImages.add(new TrainerImage("circle", image20));
        trainerImages.add(new TrainerImage("triangle", image21));
        trainerImages.add(new TrainerImage("triangle", image22));
        trainerImages.add(new TrainerImage("triangle", image23));
        trainerImages.add(new TrainerImage("triangle", image24));
        trainerImages.add(new TrainerImage("triangle", image25));
        trainerImages.add(new TrainerImage("triangle", image26));
        trainerImages.add(new TrainerImage("triangle", image27));
        trainerImages.add(new TrainerImage("triangle", image28));
        trainerImages.add(new TrainerImage("triangle", image29));
        trainerImages.add(new TrainerImage("triangle", image30));
        trainerImages.add(new TrainerImage("triangle", image31));
        trainerImages.add(new TrainerImage("triangle", image32));
        trainerImages.add(new TrainerImage("triangle", image33));
        trainerImages.add(new TrainerImage("triangle", image34));
        trainerImages.add(new TrainerImage("triangle", image35));
        trainerImages.add(new TrainerImage("triangle", image36));
        trainerImages.add(new TrainerImage("triangle", image37));
        trainerImages.add(new TrainerImage("triangle", image38));
        trainerImages.add(new TrainerImage("triangle", image39));
        trainerImages.add(new TrainerImage("triangle", image40));
        trainerImages.add(new TrainerImage("triangle", image41));
        trainerImages.add(new TrainerImage("triangle", image42));
        trainerImages.add(new TrainerImage("triangle", image43));
        trainerImages.add(new TrainerImage("triangle", image44));
        trainerImages.add(new TrainerImage("triangle", image45));
        trainerImages.add(new TrainerImage("square", image46));
        trainerImages.add(new TrainerImage("square", image47));
        trainerImages.add(new TrainerImage("square", image48));
        trainerImages.add(new TrainerImage("square", image49));
        trainerImages.add(new TrainerImage("square", image50));
        trainerImages.add(new TrainerImage("square", image51));
        trainerImages.add(new TrainerImage("square", image52));
        trainerImages.add(new TrainerImage("square", image53));
        trainerImages.add(new TrainerImage("square", image54));
        trainerImages.add(new TrainerImage("square", image55));
        trainerImages.add(new TrainerImage("square", image56));
        trainerImages.add(new TrainerImage("square", image57));
        trainerImages.add(new TrainerImage("square", image58));
        trainerImages.add(new TrainerImage("square", image59));
        trainerImages.add(new TrainerImage("square", image60));
        trainerImages.add(new TrainerImage("square", image61));
        trainerImages.add(new TrainerImage("square", image62));
        trainerImages.add(new TrainerImage("square", image63));
        trainerImages.add(new TrainerImage("square", image64));
        trainerImages.add(new TrainerImage("square", image65));
        trainerImages.add(new TrainerImage("square", image66));
        trainerImages.add(new TrainerImage("square", image67));
        trainerImages.add(new TrainerImage("square", image68));
        trainerImages.add(new TrainerImage("square", image69));
        trainerImages.add(new TrainerImage("square", image70));
        trainerImages.add(new TrainerImage("star", image71));
        trainerImages.add(new TrainerImage("star", image72));
        trainerImages.add(new TrainerImage("star", image73));
        trainerImages.add(new TrainerImage("star", image74));
        trainerImages.add(new TrainerImage("star", image75));
        trainerImages.add(new TrainerImage("star", image76));
        trainerImages.add(new TrainerImage("star", image77));
        trainerImages.add(new TrainerImage("star", image78));
        trainerImages.add(new TrainerImage("star", image79));
        trainerImages.add(new TrainerImage("star", image80));
        trainerImages.add(new TrainerImage("star", image81));
        trainerImages.add(new TrainerImage("star", image82));
        trainerImages.add(new TrainerImage("star", image83));
        trainerImages.add(new TrainerImage("star", image84));
        trainerImages.add(new TrainerImage("star", image85));
        trainerImages.add(new TrainerImage("star", image85));
        trainerImages.add(new TrainerImage("star", image86));
        trainerImages.add(new TrainerImage("star", image87));
        trainerImages.add(new TrainerImage("star", image88));
        trainerImages.add(new TrainerImage("star", image89));
        trainerImages.add(new TrainerImage("star", image90));
        trainerImages.add(new TrainerImage("star", image91));
        trainerImages.add(new TrainerImage("star", image92));
        trainerImages.add(new TrainerImage("star", image93));
        trainerImages.add(new TrainerImage("star", image94));
        trainerImages.add(new TrainerImage("star", image95));
        trainerImages.add(new TrainerImage("star", image96));
        trainerImages.add(new TrainerImage("star", image97));
        trainerImages.add(new TrainerImage("star", image98));
        trainerImages.add(new TrainerImage("star", image99));
        trainerImages.add(new TrainerImage("star", image100));
        trainerImages.add(new TrainerImage("arrow", image101));
        trainerImages.add(new TrainerImage("arrow", image102));
        trainerImages.add(new TrainerImage("arrow", image103));
        trainerImages.add(new TrainerImage("arrow", image104));
        trainerImages.add(new TrainerImage("arrow", image105));
        trainerImages.add(new TrainerImage("arrow", image106));
        trainerImages.add(new TrainerImage("arrow", image107));
        trainerImages.add(new TrainerImage("arrow", image108));
        trainerImages.add(new TrainerImage("arrow", image109));
        trainerImages.add(new TrainerImage("arrow", image110));
        trainerImages.add(new TrainerImage("arrow", image111));
        trainerImages.add(new TrainerImage("arrow", image112));
        trainerImages.add(new TrainerImage("arrow", image113));
        trainerImages.add(new TrainerImage("arrow", image114));
        trainerImages.add(new TrainerImage("arrow", image115));
        trainerImages.add(new TrainerImage("arrow", image116));
        trainerImages.add(new TrainerImage("arrow", image117));
        trainerImages.add(new TrainerImage("arrow", image118));
        trainerImages.add(new TrainerImage("arrow", image119));
        trainerImages.add(new TrainerImage("arrow", image120));
        trainerImages.add(new TrainerImage("square", image121));
        trainerImages.add(new TrainerImage("square", image122));
        trainerImages.add(new TrainerImage("square", image123));
        trainerImages.add(new TrainerImage("square", image124));
        trainerImages.add(new TrainerImage("square", image125));
        trainerImages.add(new TrainerImage("square", image126));
        trainerImages.add(new TrainerImage("square", image127));
        trainerImages.add(new TrainerImage("square", image128));
        trainerImages.add(new TrainerImage("square", image129));
        trainerImages.add(new TrainerImage("square", image130));
        return trainerImages;
    }

}
